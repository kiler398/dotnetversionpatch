﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace DotNetVersionPatch
{
    public class Version
    {
        private static readonly Regex VersionPartRegex = new Regex(@"^\d+$", RegexOptions.Compiled);

        /// <summary>
        /// The parsed major version
        /// </summary>
        /// <returns></returns>
        public int Major { get; private set; }

        /// <summary>
        /// The parsed minor version
        /// </summary>
        /// <returns></returns>
        public int Minor { get; private set; }

        /// <summary>
        /// The parsed patch version
        /// </summary>
        /// <returns></returns>
        public int Patch { get; private set; }

        public int Build { get; set; }

        public override string ToString()
        {
            return $"{Major}.{Minor}.{Patch}.{Build}";
        }


        /// <summary>
        /// Create a new instance of a SemVer based off the version string
        /// </summary>
        /// <param name="versionString">The version string to parse into a SemVer instance</param>
        /// <returns></returns>
        public static Version FromString(string versionString)
        {
            var parts = versionString.Trim().Split('.');
            if (parts.Length == 0)
            {
                throw new ArgumentException($"Malformed versionString: {versionString}", nameof(versionString));
            }

            return new Version
            {
                Major = SafeArrayGet(parts, 0),
                Minor = SafeArrayGet(parts, 1),
                Patch = SafeArrayGet(parts, 2),
            };
        }

        private static int SafeArrayGet(string[] array, int index)
        {
            if (index > array.Length - 1) return 0;

            var value = array[index];
            if (!VersionPartRegex.IsMatch(value))
            {
                throw new ArgumentException($"Malformed version part: {value}", "versionString");
            }
            return Convert.ToInt32(value);
        }
    }
}
