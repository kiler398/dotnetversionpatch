﻿using System;
using System.IO;
using System.Xml;

namespace DotNetVersionPatch
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
                throw new ArgumentNullException("cspName");

            string cspName = args[0];

            if (string.IsNullOrEmpty(cspName))
                throw new ArgumentNullException("cspName");

            if (args.Length < 2)
                throw new ArgumentNullException("buildVersion");

            int buildVersion = int.Parse(args[1]);

            if (buildVersion <= 0)
                throw new ArgumentNullException("buildVersion");
 

            CSProjectFile csProjectFile = new CSProjectFile(cspName);

            string csversion = csProjectFile.Version;
 
            Version version = Version.FromString(csversion);

            version.Build = buildVersion;
 

            csProjectFile.Version = version.ToString();

            csProjectFile.Save();

            Console.WriteLine(version);
        }
    }
}
