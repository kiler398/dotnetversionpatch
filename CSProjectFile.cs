﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DotNetVersionPatch
{
    public class CSProjectFile
    {
        private const string XMLHead = @"<?xml version='1.0' encoding='utf-8' ?>\r\n";

        private string filePath;

        private XmlDocument xmlDocument;

        public string Version {
            get
            {
                return xmlDocument.SelectSingleNode("Project/PropertyGroup/Version").InnerText;
            }
            set { xmlDocument.SelectSingleNode("Project/PropertyGroup/Version").InnerText = value; }
        } 


        public CSProjectFile(string _filePath)
        {
            string content = File.ReadAllText(_filePath);
 
            xmlDocument = new XmlDocument();

            xmlDocument.LoadXml(content);

            this.filePath = _filePath;
        }


        public void Save()
        {
 

            xmlDocument.Save(this.filePath);

 
        }
    }
}
